﻿using System;
using System.Linq;

namespace ApiRestPedidoVenda
{
    public static class UtilApi
    {
        /// <summary>
        /// Retorna a string apenas com dígitos
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public static string RetornarApenasDigitos(string texto)
        {
            if (string.IsNullOrWhiteSpace(texto))
            {
                return texto;
            }

            texto = texto.Trim();
            string.Concat(texto.Where(Char.IsDigit));

            return texto;
        }
    }
}

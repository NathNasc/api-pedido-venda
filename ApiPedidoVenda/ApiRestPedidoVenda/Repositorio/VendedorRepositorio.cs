﻿using ApiRestPedidoVenda.Models;
using ApiRestPedidoVenda.Repositorio.Interfaces;
using System;
using System.Collections.Generic;

namespace ApiRestPedidoVenda.Repositorio
{
    public class VendedorRepositorio : IVendedorRepositorio
    {
        //Criando um repositório como composição
        private readonly IRepositorioBase<VendedorModel> _repositorioVendedor;

        /// <summary>
        /// Inicializando o repositório com injeção de dependência
        /// </summary>
        /// <param name="repositorio"></param>
        public VendedorRepositorio(IRepositorioBase<VendedorModel> repositorio)
        {
            _repositorioVendedor = repositorio;
        }

        /// <summary>
        /// Inclui um vendedor
        /// </summary>
        /// <param name="entidade"></param>
        public void Incluir(VendedorModel entidade)
        {
            _repositorioVendedor.Incluir(entidade);
        }

        /// <summary>
        /// Obtem um vendedor
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public VendedorModel Obter(Guid id)
        {
            return _repositorioVendedor.Obter(id);
        }
    }
}

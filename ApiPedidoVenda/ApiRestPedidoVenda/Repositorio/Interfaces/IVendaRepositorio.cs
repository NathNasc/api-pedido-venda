﻿using ApiRestPedidoVenda.Enums;
using ApiRestPedidoVenda.Models;
using System;

namespace ApiRestPedidoVenda.Repositorio.Interfaces
{
    public interface IVendaRepositorio
    {

        VendaModel Obter(Guid Id);

        void Incluir(VendaModel entidade);

        void AlterarStatus(Guid id, StatusVenda novoStatus);

        void SalvarAlteracoes();
    }
}

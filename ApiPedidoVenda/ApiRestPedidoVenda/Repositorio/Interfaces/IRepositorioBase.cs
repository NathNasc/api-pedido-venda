﻿using ApiRestPedidoVenda.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ApiRestPedidoVenda.Repositorio.Interfaces
{
    public interface IRepositorioBase<T> where T : EntidadeBaseModel
    {
        IEnumerable<T> ObterLista(Expression<Func<T, bool>> filter = null);

        T Obter(Guid Id);

        void Incluir(T entidade);

        void Alterar(T entidade);

        void SalvarAlteracoes();
    }
}

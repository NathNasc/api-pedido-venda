﻿using ApiRestPedidoVenda.Models;
using System;
using System.Collections.Generic;

namespace ApiRestPedidoVenda.Repositorio.Interfaces
{
    public interface IItemVendaRepositorio
    {
        IEnumerable<ItemVendaModel> ObterListaPorVenda(Guid id);

        void Incluir(ItemVendaModel entidade);
    }
}

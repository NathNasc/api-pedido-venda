﻿using ApiRestPedidoVenda.Models;
using System;

namespace ApiRestPedidoVenda.Repositorio.Interfaces
{
    public interface IVendedorRepositorio
    {
        VendedorModel Obter(Guid id);

        void Incluir(VendedorModel entidade);
    }
}

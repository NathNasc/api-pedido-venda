﻿using ApiRestPedidoVenda.Enums;
using ApiRestPedidoVenda.Models;
using ApiRestPedidoVenda.Repositorio.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiRestPedidoVenda.Repositorio
{
    public class ItemVendaRepositorio : IItemVendaRepositorio
    {
        //Criando um repositório como composição
        private readonly IRepositorioBase<ItemVendaModel> _repositorioItens;

        /// <summary>
        /// Inicializando o repositório com injeção de dependência
        /// </summary>
        /// <param name="repositorio"></param>
        public ItemVendaRepositorio(IRepositorioBase<ItemVendaModel> repositorio)
        {
            _repositorioItens = repositorio;
        }

        /// <summary>
        /// Inclui o item de uma venda
        /// </summary>
        /// <param name="entidade"></param>
        /// <returns></returns>
        public void Incluir(ItemVendaModel entidade)
        {
            _repositorioItens.Incluir(entidade);
        }

        /// <summary>
        /// Retorna os itens de uma venda
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<ItemVendaModel> ObterListaPorVenda(Guid id)
        {
            return _repositorioItens.ObterLista(p => p.IdVenda == id);
        }
    }
}

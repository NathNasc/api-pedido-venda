﻿using ApiRestPedidoVenda.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiRestPedidoVenda.Repositorio
{
    public class Contexto : DbContext
    {
        public Contexto(DbContextOptions<Contexto> options) : base(options)
        {
        }

        public DbSet<VendaModel> Vendas { get; set; }
        public DbSet<ItemVendaModel> ItensVendas { get; set; }
        public DbSet<VendedorModel> Vendedores { get; set; }
    }
}

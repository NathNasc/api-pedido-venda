﻿using ApiRestPedidoVenda.Enums;
using ApiRestPedidoVenda.Models;
using ApiRestPedidoVenda.Repositorio.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPedidoVenda.Repositorio
{
    public class VendaRepositorio : IVendaRepositorio
    {
        //Criando um repositório como composição
        private readonly IRepositorioBase<VendaModel> _repositorioVenda;
        private readonly IRepositorioBase<ItemVendaModel> _repositorioItem;
        private readonly IRepositorioBase<VendedorModel> _repositorioVendedor;

        /// <summary>
        /// Inicializando o repositório com injeção de dependência
        /// </summary>
        /// <param name="repositorio"></param>
        public VendaRepositorio(IRepositorioBase<VendaModel> repositorio, IRepositorioBase<ItemVendaModel> repositorioItem, IRepositorioBase<VendedorModel> repositorioVendedor)
        {
            _repositorioVenda = repositorio;
            _repositorioItem = repositorioItem;
            _repositorioVendedor = repositorioVendedor;
        }

        /// <summary>
        /// Inclui o resumo de uma venda
        /// </summary>
        /// <param name="venda"></param>
        /// <returns></returns>
        public void Incluir(VendaModel venda)
        {
            _repositorioVenda.Incluir(venda);

            foreach (ItemVendaModel item in venda.Itens)
            {
                item.IdVenda = venda.Id;
                _repositorioItem.Incluir(item);
            }

            VendedorModel vendedorExistente = _repositorioVendedor.Obter(venda.Id);
            if (vendedorExistente == null)
            {
                _repositorioVendedor.Incluir(vendedorExistente);
            }
        }

        /// <summary>
        /// Valida e altera o status de uma venda
        /// </summary>
        /// <param name="id"></param>
        /// <param name="novoStatus"></param>
        /// <returns></returns>
        public void AlterarStatus(Guid id, StatusVenda novoStatus)
        {
            VendaModel vendaModel = _repositorioVenda.Obter(id);
            if (vendaModel == null)
            {
                throw new Exception("Venda não encontrada!");
            }

            if (!vendaModel.ValidarTrocaStatus(novoStatus))
            {
                throw new InvalidOperationException("Troca de status inválida!");
            }

            vendaModel.Status = novoStatus;
            _repositorioVenda.Alterar(vendaModel);
        }

        /// <summary>
        /// Retorna uma venda pelo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public VendaModel Obter(Guid id)
        {
            VendaModel vendaRetorno = _repositorioVenda.Obter(id);
            if (vendaRetorno != null)
            {
                vendaRetorno.Itens = _repositorioItem.ObterLista(p => p.IdVenda == id).ToList();
                vendaRetorno.Vendedor = _repositorioVendedor.Obter(vendaRetorno.IdVendedor);
            }

            return vendaRetorno;
        }

        /// <summary>
        /// Salva as alterações
        /// </summary>
        public void SalvarAlteracoes()
        {
            _repositorioVenda.SalvarAlteracoes();
        }
    }
}

﻿using ApiRestPedidoVenda.Models.Base;
using ApiRestPedidoVenda.Repositorio.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ApiRestPedidoVenda.Repositorio
{
    public class RepositorioBase<T> : IRepositorioBase<T> where T : EntidadeBaseModel
    {
        private readonly DbSet<T> _dbSetEntidade;
        private readonly Contexto _contexto;

        /// <summary>
        /// Inicializa o repositório com o tipo da entidade
        /// </summary>
        /// <param name="contexto"></param>
        public RepositorioBase(Contexto contexto)
        {
            _contexto = contexto;
            _dbSetEntidade = contexto.Set<T>();
        }

        public void Alterar(T entidade)
        {
            _dbSetEntidade.Update(entidade);
        }

        public void Incluir(T entidade)
        {
            _dbSetEntidade.AddAsync(entidade);
        }

        public T Obter(Guid id)
        {
            return _dbSetEntidade.Find(id);
        }

        public IEnumerable<T> ObterLista(Expression<Func<T, bool>> filter = null)
        {
            var query = _dbSetEntidade.AsQueryable();

            if (filter != null)
                query = query
                    .Where(filter)
                    .AsNoTracking();

            return query.ToList();
        }

        public void SalvarAlteracoes()
        {
            _contexto.SaveChangesAsync();
        }
    }
}

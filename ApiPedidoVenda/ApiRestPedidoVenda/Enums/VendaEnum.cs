﻿namespace ApiRestPedidoVenda.Enums
{
    /// <summary>
    /// Possíveis status para a venda
    /// </summary>
    public enum StatusVenda
    {
        AguardandoPagamento = 0,
        PagamentoAprovado = 1,
        EnviadoTransportadora = 2,
        Cancelado = 99,
        Entregue = 100
    }
}

﻿using ApiRestPedidoVenda.Models.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace ApiRestPedidoVenda.Models
{
    public class VendedorModel : EntidadeBaseModel
    {
        /// <summary>
        /// CPF do vendedor
        /// </summary>
        [Required(ErrorMessage = "O CPF do vendedor é obrigatório")]
        [MaxLength(11, ErrorMessage = "O CPF do vendedor deve ter até 11 caracteres")]
        public string Cpf { get; set; }

        /// <summary>
        /// Nome do vendedor
        /// </summary>
        [Required(ErrorMessage = "O nome do vendedor é obrigatório")]
        public string Nome { get; set; }

        /// <summary>
        /// E-mail do vendedor
        /// </summary>
        [EmailAddress(ErrorMessage = "O e-mail do vendedor é inválido")]
        [Required(ErrorMessage = "O e-mail do vendedor é obrigatório")]
        public string Email { get; set; }

        /// <summary>
        /// Telefone do vendedor
        /// </summary>
        [Required(ErrorMessage = "O telefone do vendedor é obrigatório")]
        [MaxLength(11, ErrorMessage = "O telefone do vendedor deve ter até 11 caracteres")]
        public string Telefone { get; set; }
    }
}

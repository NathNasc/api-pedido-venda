﻿using ApiRestPedidoVenda.Models.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace ApiRestPedidoVenda.Models
{
    public class ItemVendaModel : EntidadeBaseModel
    {
        /// <summary>
        /// Identificador da venda
        /// </summary>
        [Required(ErrorMessage = "O id da venda é obrigatório")]
        public Guid IdVenda { get; set; }

        /// <summary>
        /// Descrição do item
        /// </summary>
        [Required(ErrorMessage = "A descrição do item é obrigatória")]
        public string Descricao { get; set; }

        /// <summary>
        /// Quantidade vendida
        /// </summary>
        [Required(ErrorMessage = "A quantidade vendida do item é obrigatória")]
        [Range(1, double.MaxValue, ErrorMessage = "O item deve ter ao menos uma unidade vendida")]
        public double Quantidade { get; set; }

        /// <summary>
        /// Preco unitário do item
        /// </summary>
        [Required(ErrorMessage = "O preço unitário do item é obrigatório")]
        [Range(1, double.MaxValue, ErrorMessage = "O preço unitári do item deve ser informado")]
        public double PrecoUnitario { get; set; }
    }
}

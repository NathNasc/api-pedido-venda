﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ApiRestPedidoVenda.Models.Base
{
    public abstract class EntidadeBaseModel
    {
        [Key]
        public Guid Id { get; set; }

    }
}

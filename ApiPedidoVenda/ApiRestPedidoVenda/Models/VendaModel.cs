﻿using ApiRestPedidoVenda.Enums;
using ApiRestPedidoVenda.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiRestPedidoVenda.Models
{
    public class VendaModel : EntidadeBaseModel
    {
        /// <summary>
        /// Data de lançamento da venda
        /// </summary>
        [Required(ErrorMessage = "A data do lançamento é obrigatória")]
        public DateTime Lancamento { get; set; }

        /// <summary>
        /// Status que a venda se encontra
        /// </summary>
        public StatusVenda Status { get; set; }

        /// <summary>
        /// Id do vendedor que fez a venda
        /// </summary>
        [Required(ErrorMessage = "O id do vendedor é obrigatório")]
        public Guid IdVendedor { get; set; }

        /// <summary>
        /// Lista de itens da venda
        /// </summary>
        [MinLength(1, ErrorMessage = "A venda precisa ter ao menos um item")]
        public List<ItemVendaModel> Itens { get; set; }

        /// <summary>
        /// Vendador que intermediou a venda
        /// </summary>
        [Required(ErrorMessage = "O vendedor que intermediou a venda é obrigatório")]
        public VendedorModel Vendedor { get; set; }

        // Métodos

        /// <summary>
        /// Valida se a troca de status está válida
        /// </summary>
        /// <param name="novoStatus"></param>
        /// <returns></returns>
        public bool ValidarTrocaStatus(StatusVenda novoStatus)
        {
            switch (Status)
            {
                case StatusVenda.AguardandoPagamento:
                    if (novoStatus != StatusVenda.PagamentoAprovado &&
                        novoStatus != StatusVenda.Cancelado)
                    {
                        return false;
                    }

                    break;
                case StatusVenda.PagamentoAprovado:
                    if (novoStatus != StatusVenda.EnviadoTransportadora &&
                        novoStatus != StatusVenda.Cancelado)
                    {
                        return false;
                    }

                    break;
                case StatusVenda.EnviadoTransportadora:
                    if (novoStatus != StatusVenda.Entregue)
                    {
                        return false;
                    }

                    break;
                case StatusVenda.Cancelado:
                    return false;

                case StatusVenda.Entregue:
                    return false;
            }

            return true;
        }

    }
}

﻿using ApiRestPedidoVenda.Enums;
using ApiRestPedidoVenda.Models;
using ApiRestPedidoVenda.Repositorio.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPedidoVenda.Controllers
{
    /// <summary>
    /// API de vendas
    /// </summary>
    [ApiController]
    [Route("Venda")]
    public class VendaController : ControllerBase
    {
        private readonly ILogger<VendaController> _logger;
        private readonly IVendaRepositorio _repositorioVenda;

        /// <summary>
        /// API de vendas
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="repositorio"></param>
        public VendaController(ILogger<VendaController> logger, IVendaRepositorio repositorio)
        {
            _logger = logger;
            _repositorioVenda = repositorio;
        }

        /// <summary>
        /// Obtem uma venda pelo ID
        /// </summary>
        /// <param name="contexto"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("v1/{id}")]
        public ActionResult<VendaModel> ObterVenda([FromRoute] Guid id)
        {
            VendaModel vendaModel = _repositorioVenda.Obter(id);
            if (vendaModel == null)
            {
                _logger.LogWarning("Venda não encontrada");
                return NotFound("Venda não encontrada");
            }
            else
            {
                _logger.LogInformation($"Venda obtida com sucesso - {DateTime.Now}: {id}");
                return Ok(vendaModel);
            }
        }

        /// <summary>
        /// Altera o status de uma venda
        /// </summary>
        /// <param name="id">Id da venda a ser alterada</param>
        /// <param name="Status">Novo status a ser atribuído:
        /// somente as seguintes transições são permitidas:
        /// De: Aguardando pagamento (0) Para: Pagamento Aprovado (1)
        /// De: Aguardando pagamento (0) Para: Cancelada (99)
        /// De: Pagamento Aprovado (1) Para: Enviado para Transportadora (2)
        /// De: Pagamento Aprovado (1) Para: Cancelada (99)
        /// De: Enviado para Transportador.Para: Entregue (100)
        /// </param>
        /// <returns></returns>
        [HttpPatch]
        [Route("v1/AlterarStatus/{id}")]
        public ActionResult AlterarStatus([FromRoute] Guid id, [FromBody] StatusVenda Status)
        {
            try
            {
                _repositorioVenda.AlterarStatus(id, Status);
                _repositorioVenda.SalvarAlteracoes();

                _logger.LogInformation($"Venda obtida {DateTime.Now}: {id}");
                return Ok(Status);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Erro ao obter a venda {id} - {DateTime.Now}: {ex.Message}");
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Inclui uma venda, seus itens e o vendedor, caso não exista
        /// </summary>
        /// <param name="vendaModel"></param>
        /// <returns>Ok - id da venda criada</returns>
        [HttpPost]
        [Route("v1")]
        public ActionResult IncluirVenda([FromBody] VendaModel vendaModel)
        {
            vendaModel.Status = StatusVenda.AguardandoPagamento;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                vendaModel.IdVendedor = vendaModel.Vendedor.Id;
                _repositorioVenda.Incluir(vendaModel);

                _repositorioVenda.SalvarAlteracoes();

                _logger.LogInformation($"Venda incluída {DateTime.Now}: {vendaModel.Id}");
                return Ok(vendaModel.Id);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Erro ao incluir a venda {vendaModel.Id} - {DateTime.Now}: {ex.Message}");
                return BadRequest(ex.Message);
            }


        }
    }
}

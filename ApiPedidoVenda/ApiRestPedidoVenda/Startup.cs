using ApiRestPedidoVenda.Repositorio;
using ApiRestPedidoVenda.Repositorio.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ApiRestPedidoVenda
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddScoped(typeof(IRepositorioBase<>), typeof(RepositorioBase<>));
            services.AddScoped<IVendaRepositorio, VendaRepositorio>();
            services.AddScoped<IItemVendaRepositorio, ItemVendaRepositorio>();
            services.AddScoped<IVendedorRepositorio, VendedorRepositorio>();

            services.AddEntityFramework(Configuration);

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Teste tecnico",
                    Version = "v1",
                    Description = "Teste t�cnico pottencial"
                });

                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            GerarSwagger(app);
        }

        /// <summary>
        /// Gerar documenta��o Swagger
        /// </summary>
        /// <param name="app"></param>
        private static void GerarSwagger(IApplicationBuilder app)
        {
            app.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"/swagger/v1/swagger.json", "ApiRestPedidoVenda.Documentacao.API V1");
                    c.OAuthClientId("ApiRestPedidoVenda.Documentacao.API");
                    c.OAuthAppName("Teste tecnico");
                });
        }

    }
}

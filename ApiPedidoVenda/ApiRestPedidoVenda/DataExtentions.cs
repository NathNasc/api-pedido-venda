﻿using ApiRestPedidoVenda.Repositorio;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ApiRestPedidoVenda
{
    public static class DataExtentions
    {
        public static IServiceCollection AddEntityFramework(this IServiceCollection services, IConfiguration configuracao)
        {
           // services.AddDbContext<Contexto>();

            services.AddDbContext<Contexto>(options =>
            {
                options.UseInMemoryDatabase("TesteTecnico");
            });

            return services;
        }
    }
}

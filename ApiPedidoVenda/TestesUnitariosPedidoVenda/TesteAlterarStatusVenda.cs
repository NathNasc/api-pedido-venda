using ApiRestPedidoVenda.Enums;
using ApiRestPedidoVenda.Models;
using NUnit.Framework;

namespace TestesUnitariosPedidoVenda
{
    public class TesteAlterarStatusVenda
    {
        [Test]
        public void ValidarStatus()
        {
            VendaModel model = new VendaModel()
            {
                Status = StatusVenda.AguardandoPagamento
            };

            Assert.IsFalse(model.ValidarTrocaStatus(StatusVenda.EnviadoTransportadora));
            Assert.IsTrue(model.ValidarTrocaStatus(StatusVenda.Cancelado));
            Assert.IsTrue(model.ValidarTrocaStatus(StatusVenda.PagamentoAprovado));
            Assert.IsFalse(model.ValidarTrocaStatus(StatusVenda.Entregue));
        }
    }
}
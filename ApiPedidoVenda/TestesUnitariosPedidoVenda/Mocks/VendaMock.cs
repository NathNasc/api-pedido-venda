﻿using ApiRestPedidoVenda.Enums;
using ApiRestPedidoVenda.Models;
using System;
using System.Collections.Generic;

namespace TestesUnitariosPedidoVenda.Mocks
{
    internal static class VendaMock
    {
        internal static VendaModel RetornaVendaValidaMock()
        {
            VendedorModel vendedorModel = new VendedorModel()
            {
                Cpf = "999.999.999-99",
                Email = "teste@teste.com.br",
                Id = Guid.NewGuid(),
                Nome = "Vendedor teste",
                Telefone = "(19) 99999-9999"
            };

            ItemVendaModel mockItem = new ItemVendaModel()
            {
                Descricao = "Salgadinho",
                Id = Guid.NewGuid(),
                PrecoUnitario = 5.6,
                Quantidade = 2
            };

            VendaModel mock = new VendaModel()
            {
                Id = Guid.NewGuid(),
                Status = StatusVenda.AguardandoPagamento,
                Lancamento = DateTime.Now,
                Itens = new List<ItemVendaModel>(),
                Vendedor = vendedorModel
            };

            mock.Itens.Add(mockItem);
            return mock;
        }
    }
}
